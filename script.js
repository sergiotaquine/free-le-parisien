// ==UserScript==
// @name         Free le Parisien
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       SergioTaquine
// @match        https://www.leparisien.fr/*
// @require      https://code.jquery.com/jquery-3.5.1.min.js
// @downloadURL  https://gitlab.com/sergiotaquine/free-le-parisien/raw/master/script.js
// @updateURL    https://gitlab.com/sergiotaquine/free-le-parisien/raw/master/script.js

// ==/UserScript==

(function() {
    'use strict';
    setTimeout(async() => {


    const $target1 = $('.piano-paywall');
    const $target2 = $('.blurText');
    $target1.remove();
    $target2.removeClass( "blurText" )
    }, 3000);
})();
